package com.app.user.userapp.repository;

import com.app.user.userapp.model.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<Usuario, Integer> {
}

