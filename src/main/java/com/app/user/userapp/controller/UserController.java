package com.app.user.userapp.controller;

import com.app.user.userapp.model.Usuario;
import com.app.user.userapp.service.UserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping({"/user"})
public class UserController {
    @Autowired
    UserServices service;

    @PostMapping
    public Usuario addUser(@RequestBody Usuario usuario){
        return service.addUser(usuario);
    }

    @GetMapping("/{id}")
    public  Usuario getByIdUser(@PathVariable int id){
        return service.getUserById(id);
    }


    @GetMapping
    public List<Usuario> listar(){
        return service.getUsers();
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable int id){
        service.deleteUserById(id);
    }
}
