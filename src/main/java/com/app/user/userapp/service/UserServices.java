package com.app.user.userapp.service;

import com.app.user.userapp.model.Usuario;
import com.app.user.userapp.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServices {

    @Autowired
    private UserRepository repositorio;

    public List<Usuario> getUsers() {
        return repositorio.findAll();
    }

    public Usuario addUser(Usuario usuario) {
        return repositorio.save(usuario);
    }

    public Usuario editUser(Usuario usuario) {
        return repositorio.save(usuario);
    }

    public void deleteUserById(int id) {
        repositorio.deleteById(id);
    }

    public Usuario getUserById(int id) {
        return repositorio.findById(id).orElse(null);

    }

}
