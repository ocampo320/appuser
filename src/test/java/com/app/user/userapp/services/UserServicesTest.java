package com.app.user.userapp.services;

import com.app.user.userapp.model.Usuario;
import com.app.user.userapp.repository.UserRepository;
import com.app.user.userapp.service.UserServices;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
@ExtendWith(MockitoExtension.class)
public class UserServicesTest {
    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserServices userServices;

    @Test
    void shouldReturnFindAll() {
        List<Usuario> datas = new ArrayList();
        datas.add(new Usuario(
                1,
                "deivi",
                "lopez",
                "deivi320@hotmail.com",
                "2220718",
                "la sierra"));
        datas.add(new Usuario(
                2,
                "deivi",
                "lopez",
                "deivi320@hotmail.com",
                "2220718",
                "la sierra"));
        datas.add(new Usuario(
                3,
                "deivi",
                "lopez",
                "deivi320@hotmail.com",
                "2220718",
                "la sierra"));

        given(userRepository.findAll()).willReturn(datas);

        List<Usuario> expected = userServices.getUsers();

        assertEquals(expected, datas);
    }
    @Test
    void shouldSaveUserSuccess(){

       final  Usuario usuario=new Usuario(
                3,
                "deivi",
                "lopez",
                "deivi320@hotmail.com",
                "2220718",
                "la sierra");

       given(userRepository.save(usuario)).willAnswer(invocation->invocation.getArgument(0));
       Usuario userSave=userServices.addUser(usuario);
       assertThat(userSave).isNotNull();
       verify(userRepository).save(any(Usuario.class));

    }

    @Test
    void findUserById(){
        final int id = 1;
        final Usuario user =new Usuario(
                3,
                "deivi",
                "lopez",
                "deivi320@hotmail.com",
                "2220718",
                "la sierra");

        given(userRepository.findById(id)).willReturn(Optional.of(user));

        final Usuario expected  =userServices.getUserById(id);

        assertThat(expected).isNotNull();

    }
    @Test
    void shouldBeDelete() {
        final int userId=1;

        userServices.deleteUserById(userId);
        userServices.deleteUserById(userId);

        verify(userRepository, times(2)).deleteById(userId);
    }

    @Test
    void updateUser() {
        final Usuario user =new Usuario(
                3,
                "deivi",
                "lopez",
                "deivi320@hotmail.com",
                "2220718",
                "la sierra");

        given(userRepository.save(user)).willReturn(user);

        final Usuario expected = userServices.editUser(user);

        assertThat(expected).isNotNull();

        verify(userRepository).save(any(Usuario.class));
    }

}
